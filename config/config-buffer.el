;;; config-buffer.el --- Configure buffer related stuffs for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: buffer, ibuffer, Emacs

;;; Commentary:

;; This configures ibuffer and other buffer related things

;;; Code:

(use-package ibuffer
  :demand
  :bind (("C-x C-b" . ibuffer-list-buffers))
  :custom
  (ibuffer-saved-filter-groups
   (quote (("home"
	    ("dired" (mode . dired-mode))
	    ("erc" (or
		    (mode . erc-mode)
		    (mode . erc-list-menu-mode)))
	    ("emacs" (or
		      (name . "^\\*scratch\\*$")
		      (name . "^\\*dashboard\\*$")
		      (name . "^\\*Messages\\*$")))
	    ("emacs-lisp" (or
			   (name . "\\.*.el$")
			   (name . "\\.*.el.gz$")))
	    ("gpg" (or
		    (name . "\\.*.gpg$")))
	    ("gnus" (or
		     (mode . message-mode)
                     (mode . bbdb-mode)
                     (mode . mail-mode)
                     (mode . gnus-group-mode)
                     (mode . gnus-summary-mode)
                     (mode . gnus-article-mode)
                     (name . "^\\.bbdb$")
		     (name . "^\\*Server\\*$")
		     (name . "^\\*Gnus[[:print:]]\\*$")
                     (name . "^\\.newsrc-dribble")))
	    ("vc" (or
		   (name . "*vc*")))
	    ("flymake" (or
			(name . "^\\*Flymake[[:blank:]][[:print:]]+\\*$")))
	    ("org" (mode . org-mode))
	    ("help" (or
		     (name . "^\\*Help\\*$")
		     (name . "^\\*info\\*$")
		     (name . "[[:print:]]+\\.el.gz")))
	    ("shell" (or
		      (mode . eshell-mode)
		      (mode . shell-mode)
		      (mode . sh-mode)
		      (name . "^\\*Async[[:space:]]*")
		      (name . "^\\*Eshell[[:print:]]+\\*$")
		      (name . "^\\*terminal\\*$")
		      (name . "^\\*shell\\*$")
		      (name . "^\\*ansi-term\\*$")))
	    ("change-log" (mode . change-log-mode))
	    ("tags" (mode . tags-table-mode))
	    ("ref" (mode . xref--xref-buffer-mode))
	    ("compile" (or
			(mode . compilation-mode)
			(mode . emacs-lisp-compilation-mode)))
	    ("web" (or
		    (mode . js-mode)
		    (mode . mhtml-mode)
		    (mode . css-mode)))
	    ("regex" (or
		      (mode . occur-mode)
		      (mode . reb-mode)))
	    ("make" (or
		     (mode . makefile-bsdmake-mode)))
	    ("conf" (or
		     (mode . conf-space-mode)))
	    ("info" (or
		     (mode . texinfo-mode)
		     (mode . TeX-texinfo-mode)
		     (mode . Info-mode)
		     (mode . markdown-mode)))
	    ("proced" (mode . proced-mode))
	    ("text" (mode . fundamental-mode))
	    ("eww" (mode . eww-mode))
	    ("customization" (mode . Custom-mode))
	    ("log" (or
		    (name . "^\\*Backtrace\\*$")
		    (name . "^\\*Completions\\*$")
		    (name . "^\\*Utils Log\\*$")
		    (name . "^\\*Warnings\\*$")))))))
  :hook
  (ibuffer-mode . (lambda ()
		    (ibuffer-auto-mode 1)
		    (ibuffer-switch-to-saved-filter-groups "home"))))

(provide 'config-buffer)

;;; config-buffer.el ends here
