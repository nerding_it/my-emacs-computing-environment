;;; config-tls.el --- Configure gnutls for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: tls, elegant

;;; Commentary:

;; This is configuration related to gnutls

;;; Code:

(use-package gnutls
  :demand t
  :custom
  (gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3"))

(provide 'config-tls)

;;; config-tls.el ends here
