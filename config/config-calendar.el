;;; config-calendar.el --- Configure calendar for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: calendar, Emacs, elegant

;;; Commentary:

;; This is configuration related to calendar

;;; Code:

(use-package calendar
  :demand t
  :custom
  (calendar-latitude 12.9)
  (calendar-longitude 77.5)
  (calendar-location-name "Bengaluru, IN"))

(provide 'config-calendar)

;;; config-calendar.el ends here
