;;; config-request.el --- This is configuration related to requests package

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 11 May 2021
;; Keywords: Emacs, requests

;;; Commentary:

;; This is configuration related to request package

;;; Code:

(use-package request
  :ensure)

(provide 'config-request)

;;; config-request.el ends here
