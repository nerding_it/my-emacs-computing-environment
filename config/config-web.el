;;; config-web.el --- Configure web dev related packages for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: web, web development, Emacs

;;; Commentary:

;; This configures packages that is related to web-development

;;; Code:

(use-package csharp-mode
  :ensure t
  :mode "\\.cs\\'")

(use-package mhtml-mode
  :mode "\\.html\\'")

(use-package typescript-mode
  :ensure t
  :mode "\\.ts\\'")

(use-package ob-typescript
  :ensure t
  :after (org)
  :requires (typescript-mode)
  :config
  (org-babel-do-load-languages 'org-babel-load-languages
			       (append org-babel-load-languages
				       '((typescript . t)))))

(use-package flymake-eslint
  :ensure)

(use-package js-doc
  :ensure)

(use-package nvm
  :ensure t)

(use-package js-comint
  :ensure t
  :after nvm
  :config
  (js-do-use-nvm))

(use-package js
  :requires (flymake-eslint js-doc)
  :bind (("C-c C-r" . js-comint-send-region)
	 ("C-c C-c" . js-comint-send-buffer)
	 ("C-c C-e" . js-comint-send-last-sexp))
  :hook ((js-mode . flymake-eslint-enable)
	 (js-mode . refresh-symbols)
	 (js-mode . (lambda ()
		      (define-key js-mode-map "\C-ci" 'js-doc-insert-function-doc)
		      (define-key js-mode-map "@" 'js-doc-insert-tag))))
  :custom
  (js-indent-level 2)
  (org-babel-js-function-wrapper
      "console.log(require('util').inspect(function(){\n%s\n}(), { depth: 100 }))")
  :config
  (add-to-list 'auto-mode-alist '("\\.mjs\\'" . js-mode))
  (remove-hook 'flymake-diagnostic-functions 'flymake-proc-legacy-flymake)
  (org-babel-do-load-languages 'org-babel-load-languages
			       (append org-babel-load-languages
				       '((js . t)))))

(use-package css-mode
  :mode ("\\.css\\'" "\\.scss\\'")
  :bind ("C-h C-s" . css-lookup-symbol)
  :custom
  (css-indent-offset 2))

(use-package restclient
  :ensure
  :config
  (add-to-list 'auto-mode-alist '("\\.http\\'" . restclient-mode)))

(use-package ob-restclient
  :ensure
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((restclient . t))))

(use-package vue-mode
  :ensure
  :mode ("\\..vue\\'"))

(use-package markdown-mode
  :ensure)

(use-package nvm
  :quelpa ((nvm
	    :fetcher github
	    :repo "rejeep/nvm.el")
	   :upgrade nil)
  :config
  (defun mine/nvm-use ()
    "Wrapper for nvm.el."
    (interactive)
    (let ((version (ido-completing-read "Version: " (seq-map
						     (lambda (ver) (car ver))
						     (nvm--installed-versions)))))
      (nvm-use version))))

(provide 'config-web)

;;; config-web.el ends here
