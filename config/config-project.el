;;; config-project.el --- Configure projectile for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: project, elegant

;;; Commentary:

;; This is configuration related to projectile for Emacs.

;;; Code:

(use-package projectile
  :ensure t
  :config
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (projectile-mode))

(provide 'config-project)

;;; config-project.el ends here
