;;; config-info.el --- Configuration for info reader

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 14 Jun 2021
;; Keywords: Emacs, Info

;;; Commentary:

;; Configuration for Info reader

;;; Code:

(use-package info
  :config
  (add-to-list 'Info-default-directory-list (expand-file-name "share/info/git" user-emacs-directory))
  (add-to-list 'Info-default-directory-list (expand-file-name "share/info/nodejs" user-emacs-directory))
  (add-to-list 'Info-default-directory-list (expand-file-name "share/info/rust" user-emacs-directory))
  (dolist (dir (directory-files (expand-file-name "elpa" user-emacs-directory)))
    (when (and (not (equal dir ".")) (not (equal dir "..")))
      (if (file-exists-p (expand-file-name "dir" (expand-file-name dir (expand-file-name "elpa" user-emacs-directory))))
	  (add-to-list 'Info-default-directory-list (expand-file-name "dir" (expand-file-name dir (expand-file-name "elpa" user-emacs-directory)))))))
  :hook (Info-mode . (lambda ()
		       (setq Info-additional-directory-list Info-default-directory-list))))

(provide 'config-info)

;;; config-info.el ends here
