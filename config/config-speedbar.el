;;; config-speedbar.el --- Configuration for speedbar

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 28 Oct 2020
;; Keywords: speedbar, Emacs

;;; Commentary:

;; This is configuration for speedbar

;;; Code:

(use-package sr-speedbar
  :ensure)

(use-package speedbar
  :custom
  (speedbar-use-imenu-flag nil)
  :hook (speedbar-load . (lambda ()
			   (require 'semantic/sb))))

(provide 'config-speedbar)

;;; config-speedbar.el ends here
