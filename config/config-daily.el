;;; config-daily.el --- Configure general workflow for day to day tasks

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: workflow, elegant

;;; Commentary:

;; This is configuration related to general workflow
;; for example update, upgrade, checking for upgrade and many more

;;; Code:

(defun mine/update ()
  "Update package repository."
  (interactive)
  (async-shell-command "sudo apt-get update"))

(defun mine/upgradable ()
  "Check for available update."
  (interactive)
  (async-shell-command "apt list --upgradable"))

(defun mine/upgrade ()
  "Upgrade packages."
  (interactive)
  (async-shell-command "sudo apt-get upgrade"))

(defun mine/visit-git ()
  "Visit working directory."
  (interactive)
  (dired mine-git-directory))

(defun mine/clone-project ()
  "Clone project to `MINE-GIT-DIRECTORY'."
  (interactive)
  (let* ((default-directory mine-git-directory)
	 (output-buffer "*Project Clone*")
	 (url (read-string "Please enter URL: "))
	 (file-name (replace-regexp-in-string ".git" "" (replace-regexp-in-string "[a-z]+@[a-z]+.[a-z]+:[a-z-_]+/" "" url)))
	 (proc (progn
		 (async-shell-command (format "git clone %s" url) output-buffer)
		 (add-to-list 'bookmark-alist `(,file-name (filename . ,(expand-file-name file-name mine-git-directory))))
		 (get-buffer-process output-buffer))))
    (if (process-live-p proc)
	(set-process-sentinel proc
			      (lambda (process signal)
				(when (memq (process-status process) '(exit signal))
				  (kill-buffer "*Project Clone*")))))))

(global-set-key (kbd "C-c g") 'mine/visit-git)

(provide 'config-daily)

;;; config-daily.el ends here
