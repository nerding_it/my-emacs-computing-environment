;;; config-ledger.el --- Configuration for ledger

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 28 Oct 2020
;; Keywords: ledger, accounting, plain text accounting

;;; Commentary:

;; This is configuration for ledger mode

;;; Code:

(use-package ledger-mode
  :ensure t
  :config
  (org-babel-do-load-languages 'org-babel-load-languages
			       (append org-babel-load-languages
				       '((ledger . t)))))

(provide 'config-ledger)

;;; config-ledger.el ends here

