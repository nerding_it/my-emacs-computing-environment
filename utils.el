;;; utils.el --- Utilities function for emacs configuration

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 12 May 2021
;; Keywords: utils, emacs, logging

;;; Commentary:

;; This will contain all utilities function used in emacs configuration

;;; Code:


(define-derived-mode utils-log-mode fundamental-mode "Utils Log")

;; TODO: Need to implement type checking for different type of objects
(defun utils-log (level msg)
  "Log level and messages to log buffer."
  (get-buffer-create "*Utils Log*")
  (cond ((equal level "DEBUG")
	 (with-current-buffer "*Utils Log*"
	   (utils-log-mode)
	   (insert (propertize (format "%s %s\n" (format-time-string "%D %H:%M:%S") msg) 'face '(:foreground "purple")))))
	((equal level "VERBOSE")
	 (with-current-buffer "*Utils Log*"
	   (utils-log-mode)
	   (insert (propertize (format "%s %s\n" (format-time-string "%D %H:%M:%S") msg) 'face '(:foreground "blue")))))
	((equal level "INFO")
	 (with-current-buffer "*Utils Log*"
	   (utils-log-mode)
	   (insert (propertize (format "%s %s\n" (format-time-string "%D %H:%M:%S") msg) 'face '(:foreground "dark green")))))
	((equal level "NOTICE")
	 (with-current-buffer "*Utils Log*"
	   (utils-log-mode)
	   (insert (propertize (format "%s %s\n" (format-time-string "%D %H:%M:%S") msg) 'face '(:foreground "violet")))))
	((equal level "WARNING")
	 (with-current-buffer "*Utils Log*"
	   (utils-log-mode)
	   (insert (propertize (format "%s %s\n" (format-time-string "%D %H:%M:%S") msg) 'face '(:foreground "orange")))))
	((equal level "SUCCESS")
	 (with-current-buffer "*Utils Log*"
	   (utils-log-mode)
	   (insert (propertize (format "%s %s\n" (format-time-string "%D %H:%M:%S") msg) 'face '(:foreground "light green")))))
	((equal level "ERROR")
	 (with-current-buffer "*Utils Log*"
	   (utils-log-mode)
	   (insert (propertize (format "%s %s\n" (format-time-string "%D %H:%M:%S") msg) 'face '(:foreground "dark red")))))
	((equal level "CRITICAL")
	 (with-current-buffer "*Utils Log*"
	   (utils-log-mode)
	   (insert (propertize (format "%s %s\n" (format-time-string "%D %H:%M:%S") msg) 'face '(:foreground "red")))))))

(provide 'utils)

;;; utils.el ends here
