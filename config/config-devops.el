;;; config-ido.el --- Configure devops related stuffs for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: devops, docker

;;; Commentary:

;; This is configuration related to devops

;;; Code:

(use-package docker
  :ensure t
  :bind ("C-c d" . docker))

(provide 'config-devops)

;;; config-devops.el ends here
