;;; config-core.el --- Configure core for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: core, elegant

;;; Commentary:

;; This is configuration related to general packages

;;; Code:

(use-package emacs
  :demand t
  :custom
  (tab-always-indent 'complete)
  (inhibit-splash-screen t)
  (inhibit-startup-message t)
  (create-lockfiles nil)
  (browse-url-browser-function 'eww-browse-url))

(use-package simple
  :custom
  (kill-ring-max (* 4 2048))
  (global-mark-ring-max 1024))

(defun mine/exit-emacs ()
  "Quit emacs."
  (interactive)
  (message "You can't quit emacs"))

(global-unset-key (kbd "C-x C-c"))
(global-set-key (kbd "C-x C-c") 'mine/exit-emacs)

(require 'subr-x)
(setq user-full-name (string-trim (shell-command-to-string "git config --global user.name")))
(setq user-mail-address (string-trim (shell-command-to-string "git config --global user.email")))

(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)
(prefer-coding-system 'utf-8)
(setq indent-tabs-mode nil)

(defalias 'yes-or-no-p 'y-or-n-p)

(use-package files
  :custom
  (delete-old-versions -1)
  (backup-directory-alist `((".*" . ,mine-cache-directory)))
  (version-control t)
  (auto-save-file-name-transforms `((".*" , mine-cache-directory t))))

(provide 'config-core)

;;; config-core.el ends here
