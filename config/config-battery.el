;;; config-battery.el --- Configure battery for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: battery, elegant

;;; Commentary:

;; This is configuration related to battery

;;; Code:

(use-package battery
  :config
  (display-battery-mode))

(provide 'config-battery)

;;; config-battery.el ends here
