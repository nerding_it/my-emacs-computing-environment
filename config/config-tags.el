;;; config-tags.el --- Configure tags using etags

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 27 Oct 2020
;; Keywords: etags, Emacs

;;; Commentary:

;; This is configuration related to etags

;;; Code:

(use-package etags
  :config
  (defun mine/ido-find-tag ()
    "Find a tag using ido."
    (interactive)
    (tags-completion-table)
    (let (tag-names)
      (mapcar (lambda (x)
                  (push (prin1-to-string x t) tag-names))
                tags-completion-table)
      (find-tag (ido-completing-read "Tag: " tag-names))))
  (defun mine/build-tags (dir-name)
    "Build tags file."
    (interactive "DDirectory: ")
    (setq exts (read-string "Extensions: "))
    (eshell-command
     (replace-regexp-in-string "\\\\" "" (format "find %s -type f -name \"*.%s\" -print | etags -" dir-name exts)))
    (visit-tags-table (expand-file-name "TAGS" dir-name))))

(provide 'config-tags)

;;; config-tags.el ends here

