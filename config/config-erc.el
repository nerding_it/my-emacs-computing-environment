;;; config-erc.el --- Configure erc for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: erc, elegant

;;; Commentary:

;; This is configuration related to erc for Emacs

;;; Code:

(use-package erc
  :custom
  (erc-nick "nerding_it")
  (erc-autojoin-channels-alist '(("#emacs" "#bitcoin" "#org-mode")))
  (erc-autojoin-timing 'ident)
  (erc-fill-static-center 22)
  (erc-hide-list '("JOIN" "PART" "QUIT"))
  (erc-lurker-hide-list '("JOIN" "PART" "QUIT"))
  (erc-lurker-threshold-time 43200)
  (erc-default-server "irc.libera.chat")
  (erc-default-port 6667)
  (erc-prompt-for-nickserv-password nil)
  (erc-server-reconnect-attempts 5)
  (erc-server-reconnect-timeout 3)
  (erc-track-exclude-types '("JOIN" "MODE" "NICK" "PART" "QUIT"
			     "324" "329" "332" "333" "353" "477"))
  (erc-prompt-for-password nil)
  :config
  (add-to-list 'erc-modules 'notifications)
  (add-to-list 'erc-modules 'spelling)
  (erc-services-mode 1)
  (erc-update-modules))

(provide 'config-erc)

;;; config-erc.el ends here
