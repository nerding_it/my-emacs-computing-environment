;;; config-system.el --- Configure system (i.e. OS related) for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 16 Oct 2020
;; Keywords: system, elegant

;;; Commentary:

;; This is configuration related to operating system specific for Emacs

;;; Code:

(defvar os/package-manager-program nil
  "Program which manages packages.")

(use-package exec-path-from-shell
  :demand t
  :ensure t
  :config
  (exec-path-from-shell-initialize))

(cond ((eq system-type 'darwin)
       (setq os/package-manager-program "brew"))
      ((eq system-type 'gnu/linux)
       (setq os/package-manager-program "apt")))

(defun os/package-search ()
  "Search for packages."
  (interactive)
  (let* ((output-buffer (get-buffer-create "*Package Search Results*")))
    (async-shell-command (format "%s search %s" os/package-manager-program (read-string "Package: ")) output-buffer)))

(defun os/package-info ()
  "Display info about the packages."
  (interactive)
  (let* ((package (read-string "Package: "))
	 (output-buffer (get-buffer-create (format "*Package Info for %s*" package))))
    (async-shell-command (format "%s info %s" os/package-manager-program package) output-buffer)))

(defun os/package-install ()
  "Install the package."
  (interactive)
  (let* ((package (read-string "Package: "))
	 (output-buffer (get-buffer-create "*Package Install*")))
    (async-shell-command (format "%s install %s" os/package-manager-program package) output-buffer)))

(defun os/package-uninstall ()
  "Uninstall the package."
  (interactive)
  (let* ((package (read-string "Package: "))
	 (output-buffer (get-buffer-create "*Package Uninstall*")))
    (async-shell-command (format "%s remove %s" os/package-manager-program package) output-buffer)))

(provide 'config-system)

;;; config-system.el ends here
