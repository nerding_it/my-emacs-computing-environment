;;; config-newsticker.el --- Configure newsticker for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: newsticker, elegant

;;; Commentary:

;; This is configuration related to news ticker

;;; Code:

(use-package newsticker
  :custom
  (newsticker-retrieval-interval 0)
  (newsticker-automatically-mark-items-as-old nil)
  (newsticker-frontend 'newsticker-treeview)
  (newsticker-dir (expand-file-name "news/" mine-cache-directory))
  (newsticker-url-list-defaults '(("r-emacs" "https://www.reddit.com/r/emacs/.rss" nil 3600)
				  ("r-orgmode" "https://www.reddit.com/r/orgmode/.rss" nil 3600)
				  ("hn-front" "https://hnrss.org/frontpage" nil 3600)
				  ("hn-new" "https://hnrss.org/newest" nil 3600)
				  ("hn-best" "https://hnrss.org/best" nil 3600)
				  ("hn-launches" "https://hnrss.org/launches" nil 3600)
				  ("hn-jobs" "https://hnrss.org/jobs" nil 3600)
				  ("hn-emacs" "https://hnrss.org/newest?q=emacs" nil 3600))))

(provide 'config-newsticker)

;;; config-newsticker.el ends here
