;;; config-proced.el --- Configuration for proced

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 19 Jun 2021
;; Keywords: proced, Emacs, Elegant

;;; Commentary:

;; This is configuration related to proced

;;; Code:

(use-package proced
  :bind
  (("C-x p" . proced))
  :custom
  (proced-format 'verbose)
  (proced-auto-update-flag t))

(provide 'config-proced)

;;; config-proced.el ends here
