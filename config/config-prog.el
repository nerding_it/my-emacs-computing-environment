;;; config-prog.el --- Configure programming related stuffs for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: prog, flymake, Emacs

;;; Commentary:

;; This configures programming related stuffs

;;; Code:

(use-package hl-todo
  :ensure
  :custom
  (hl-todo-keyword-faces
   '(("TODO"   . "#FF0000")
     ("FIXME"  . "#FF0000")
     ("DEBUG"  . "#A020F0")
     ("GOTCHA" . "#FF4500")
     ("STUB"   . "#1E90FF"))))

(use-package prog-mode
  :requires hl-todo
  :hook (prog-mode . (lambda ()
		       (hl-todo-mode)
		       (display-line-numbers-mode 1)
		       (show-paren-mode 1)
		       (electric-pair-mode 1)
		       (flymake-mode 1))))

(use-package flymake
  :config
  :bind (("M-n" . flymake-goto-next-error)
	 ("M-p" . flymake-goto-prev-error)
	 ("C-c ! n" . flymake-goto-next-error)
	 ("C-c ! p" . flymake-goto-prev-error)
	 ("C-c ! l" . flymake-show-diagnostics-buffer)))

(provide 'config-prog)

;;; config-prog.el ends here
