;;; config-shell.el --- Configure shell for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: shell, Emacs

;;; Commentary:

;; This is configuration related to shell

;;; Code:

(use-package ob-shell
  :requires (shell-mode)
  :after (org)
  :config
  (org-babel-do-load-languages 'org-babel-load-languages
			       (append org-babel-load-languages
				       '((shell . t)))))

(provide 'config-shell)

;;; config-shell.el ends here
