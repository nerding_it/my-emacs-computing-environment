;;; config-tex.el --- Configure tex for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: tex, elegant

;;; Commentary:

;; This is configuration related to tex

;;; Code:

(use-package pdf-tools
  :ensure)

(use-package tex-site
  :requires (pdf-tools)
  :ensure auctex
  :mode (("\\.text\\'" . latex-mode)
	 ("\\.latex\\'" . latex-mode))
  :config
  (setq-default TeX-master t)
  :custom
  (TeX-auto-save t)
  (TeX-parse-self t)
  (TeX-view-program-selection '((output-pdf "pdf-tools")))
  (TeX-source-correlate-start-server t)
  (TeX-view-program-list '(("pdf-tools" "TeX-pdf-tools-sync-view")))
  :hook ((LaTeX-mode . visual-line-mode)
	 (LaTeX-mode . flyspell-mode)
	 (LaTeX-mode . LaTeX-math-mode)
	 (LaTeX-mode . turn-on-reftex)))

(provide 'config-tex)

;;; config-tex.el ends here
