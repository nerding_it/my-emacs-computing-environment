;;; config-elisp.el --- Configure elisp for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: elisp, elegant

;;; Commentary:

;; This is configuration related to Emacs Lisp mode

;;; Code:

(use-package elisp-format
  :ensure)

(use-package elisp-mode
  :hook (emacs-lisp-mode . (lambda)
			 (semantic-mode)
			 (checkdoc-minor-mode)))

(provide 'config-elisp)

;;; config-elisp.el ends here
