;;; config-conf.el --- Configuration for conf-mode

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 17 Oct 2020
;; Keywords: conf-mode, configurations, Emacs

;;; Commentary:

;; This configures conf-mode for generic config files

;;; Code:

(use-package conf-mode
  :mode ("\\.service\\'" "\\.*rc$\\'"))

(use-package conf-space-mode
  :mode ("\\Dockerfile\\'"))

(use-package yaml-mode
  :ensure
  :mode ("\\.yaml'" "\\.yml'"))

(provide 'config-conf)

;;; config-conf.el ends here
