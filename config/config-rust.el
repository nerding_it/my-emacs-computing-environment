;;; config-rust.el --- Configure rust lang for elegant-computing-environment -*- lexical-binding: t

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: abbrev, elegant

;;; Commentary:

;; This is configuration related to rust-mode

;;; Code:

(defvar flymake-rust--process nil
  "Internal variable.
Handle to the linter process for the current buffer.")

(defvar flymake-rust--message-regex "^\\(\\warning?[[:print:]]+\\|error?[[:print:]]+\\):[[:blank:]]\\([[:print:]]+\\)[[:cntrl:]]+[[:blank:]]+[[:print:]]+[[:blank:]]+\\([[:print:]]+.rs\\):\\([[:digit:]]+\\):\\([[:digit:]]+\\)$"
  "Internal variable.
Regular expression definition to match cargo check messages.")

(defcustom flymake-rust-executable-name "cargo"
  "Name of executable to run when checker is called.  Must be present in variable `exec-path'."
  :type 'string
  :group 'flymake-rust)

(defcustom flymake-rust-executable-args "check"
  "Extra arguments to pass to `FLYMAKE-RUST-EXECUTABLE-NAME'."
  :type 'string
  :group 'flymake-rust)

(defun rust-flymake-backend (report-fn &rest args)
  "Run flymake for `REPORT-FN' and `ARGS'."
  (flymake-log :debug "Starting flymake rust backend")
  (when (process-live-p flymake-rust--process)
    (kill-process flymake-rust--process))
  (let ((source (current-buffer)))
    (save-restriction
      (widen)
      (setq
       flymake-rust--process
       (make-process
	:name "rust-flymake" :noquery t :connection-type 'pipe
	:buffer (generate-new-buffer " *rust flymake*")
	:command `(,flymake-rust-executable-name ,flymake-rust-executable-args)
	:sentinel
	(lambda (proc event)
	  (when (eq 'exit (process-status proc))
	    (unwind-protect
		(if (with-current-buffer source (eq proc flymake-rust--process))
		    (with-current-buffer (process-buffer proc)
		      (goto-char (point-min))
		      (cl-loop
		       while (search-forward-regexp
			      flymake-rust--message-regex
			      nil t)
		       ;; TODO: Do more regex search to format the error message
		       for msg = (match-string 2)
		       for msg-type = (match-string 1)
		       for (beg . end) = (flymake-diag-region
					  source
					  (string-to-number (match-string 4)))
		       for type = (if (string-match "warning" msg-type)
				      :warning
				    :error)
		       collect (flymake-make-diagnostic source
							beg
							end
							type
							msg)
		       into diags
		       finally (funcall report-fn diags)))
		  (flymake-log :warning "Cancelling obsolete check %s" proc))
	      (kill-buffer (process-buffer proc)))))))
      (process-send-eof flymake-rust--process))))

(use-package rust-mode
  :ensure t
  :mode "\\.rs\\'"
  :config
  :hook (rust-mode . (lambda ()
		       (remove-hook 'flymake-diagnostic-functions 'flymake-proc-legacy-flymake)
		       (add-hook 'flymake-diagnostic-functions 'rust-flymake-backend))))

(use-package ob-rust
  :ensure t
  :requires rust-mode
  :after org
  :config
  (org-babel-do-load-languages 'org-babel-load-languages
			       (append org-babel-load-languages
				       '((rust . t)))))

(provide 'config-rust)

;;; config-rust.el ends here
