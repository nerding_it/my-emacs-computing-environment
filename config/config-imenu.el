;;; config-imenu.el --- Configure imenu

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 19 Apr 2021
;; Keywords: imenu, ref

;;; Commentary:

;; This is configuration related to imenu

;;; Code:

(use-package imenu
  :custom
  (imenu-auto-rescan t))

(provide 'config-imenu)

;;; config-imenu.el ends here
