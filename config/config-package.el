;;; config-package.el --- Configure package for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: package, Emacs

;;; Commentary:

;; This is configuration related to packages

;;; Code:

(use-package package
  :demand t
  :custom
  (package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
		      ("melpa" . "https://melpa.org/packages/")))
  :config
  (package-initialize))

(use-package quelpa-use-package
  :ensure)

(provide 'config-package)

;;; config-package.el ends here
