;;; config-diagrams.el --- Configure diagrams related stuffs for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: diagrams, plantuml, Emacs

;;; Commentary:

;; This is configuration related to diagrams

;;; Code:

(use-package plantuml-mode
  :ensure
  :custom
  (plantuml-default-exec-mode 'executable)
  (plantuml-executable-path "plantuml")
  (org-plantuml-exec-mode 'plantuml)
  (org-plantuml-executable-path plantuml-executable-path)
  :config
  (org-babel-do-load-languages 'org-babel-load-languages
			       (append org-babel-load-languages
				       '((plantuml . t)))))

(provide 'config-diagrams)

;;; config-diagrams.el ends here
