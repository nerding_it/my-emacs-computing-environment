;;; config-windows.el --- Configure windows for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: windows, elegant

;;; Commentary:

;; This is configuration related to windows

;;; Code:

(use-package windmove
  :requires register
  :bind (("s-h" . windmove-left)
	 ("s-l" . windmove-right)
	 ("s-k" . windmove-up)
	 ("s-j" . windmove-down)
	 ("s-w" . window-configuration-to-register))
  :config
  (windmove-default-keybindings 'meta))

(use-package winner
  :custom
  (winner-ring-size 1024)
  (winner-boring-buffers '("*Completions*"
			   "*Help"
			   "*Apropos*"
			   "*Buffer List*"
			   "*Compile Log*"
			   "*Calendar*"
			   "*Warnings*"))
  :bind (("<XF86Back>" . winner-undo)
	 ("<XF86Forward>" . winner-redo)
	 ("s-u" . winner-undo))
  :init
  (winner-mode))

(use-package named-window
  :quelpa ((named-window
	    :fetcher gitlab
	    :repo "nerding_it/emacs-named-window")
	   :upgrade nil))

(provide 'config-windows)

;;; config-windows.el ends here
