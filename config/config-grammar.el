;;; config-grammar.el --- Configure grammar for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: grammar, elegant

;;; Commentary:

;; This is configuration related to abbreviation feature of Emacs.

;;; Code:

(use-package flyspell
  :hook ((text-mode . flyspell-mode)
	 (prog-mode . flyspell-prog-mode))
  :config
  (define-derived-mode dict-output-mode org-mode "Dict Output")
  (defun mine/search-meaning ()
    "Search meaning for word using dict."
    (interactive)
    (let ((meaning-for (ido-completing-read "Word: " (ispell-lookup-words (or (thing-at-point 'word nil) "")))))
      (get-buffer-create "*Dict Output*")
      (switch-to-buffer "*Dict Output*")
      (read-only-mode)
      (dict-output-mode)
      (erase-buffer)
      (let ((inhibit-read-only t))
	(insert (shell-command-to-string (format "dict %s" meaning-for)))))))

(provide 'config-grammar)

;;; config-grammar.el ends here
