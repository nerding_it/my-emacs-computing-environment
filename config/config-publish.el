;;; config-publish.el --- Configure org-mode publishing for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: publish, org-mode,  Emacs

;;; Commentary:

;; This configures publishing functionality for org-mode
;; TODO: Need to write function for generating timesheets, invoices, progress report

;;; Code:

(use-package ox-rss)

(use-package htmlize
  :ensure)

(use-package ob-shell
  :requires shell-mode)

(defvar mine/website-html-head
  (f-read-text (expand-file-name "config/org-publish/head.html" user-emacs-directory) 'utf-8))
(defvar mine/website-html-postamble
  (f-read-text (expand-file-name "config/org-publish/footer.html" user-emacs-directory) 'utf-8))
(defvar mine/website-html-preamble
  (f-read-text (expand-file-name "config/org-publish/header.html" user-emacs-directory) 'utf-8))

(use-package ox-publish
  :after htmlize
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((shell . t)))

  (defun mine/org-html-sitemap-format-entry (entry style project)
    "Return string for each ENTRY in `PROJECT'."
    (when (and (not (string-equal entry "about.org")) (not (string-equal entry "contact.org")))
      (format "@@html:<span class='archive-item'><span class='archive-date'>@@ %s @@html:</span>@@ [[file:%s][%s]] @@html:</span>@@"
	      (format "<%s>" (car (org-publish-find-property entry :date project)))
	      entry
	      (org-publish-find-title entry project))))

  (defun mine/org-html-sitemap-function (title list)
    "Return sitemap using `TITLE' and `LIST' returned by `org-blog-sitemap-format-entry'."
    (when (cl-search "🖥" title)
      (setq title ""))
    (concat "#+TITLE: " title "\n\n"
	    "\n#+begin_archive\n"
	    (mapconcat (lambda (li)
			 (format "@@html:<li>@@ %s @@html:</li>@@" (car li)))
		       (seq-filter #'car (cdr list))
		       "\n")
	    "\n#+end_archive\n"))

  (defun mine/org-html-publish-to-html (plist filename pub-dir)
    "Custom function for publishing to html."
    (let ((file-path (org-html-publish-to-html plist filename pub-dir)))
      (with-current-buffer (find-file-noselect file-path)
	(goto-char (point-min))
	(search-forward "<body>")
	(insert (concat "<div class='content-wrapper container-fluid'>"
			"<div class='row'>"
			"<div class='col-9'>"))
	(goto-char (point-max))
	(search-backward "</body>")
	(insert "</div>")
	(insert (f-read-text (expand-file-name "config/org-publish/left.html" user-emacs-directory) 'utf-8))
	(insert "</div></div>")
	(goto-char (point-min))
	(search-forward "<body>")
	(while (search-forward "<img src" nil t)
	  (forward-char 2)
	  (insert "/"))
	(goto-char (point-min))
	(if (not (string-equal filename "index.org"))
	    (delete-matching-lines "class=\"title-home\""))
	(save-buffer)
	(kill-buffer))
      file-path))

  (defun mine/org-rss-publish-to-rss (plist filename pub-dir)
    "Publish rss with `plist' to `pub-dir'."
    (org-rss-publish-to-rss plist filename pub-dir))

  (defun mine/org-rss-sitemap-function (title list)
    (concat "#+TITLE: " title "\n\n"
            (org-list-to-subtree list nil '(:icount "" :istart ""))))

  (defun mine/org-rss-sitemap-format-entry (entry style project)
    (cond ((not (directory-name-p entry))
           (let* ((file (org-publish--expand-file-name entry project))
                  (title (org-publish-find-title entry project))
                  (date (format-time-string "%Y-%m-%d" (org-publish-find-date entry project)))
                  (link (concat (file-name-sans-extension entry) ".html")))
             (with-temp-buffer
	       (insert (format "* %s\n" (car-safe (org-publish-find-property entry :title project))))
               (org-set-property "RSS_PERMALINK" link)
               (org-set-property "PUBDATE" date)
	       ;; FIXME: Adding properties also
               ;; (insert-file-contents file)
	       (insert (format "%s" (car-safe (org-publish-find-property entry :title project))))
               (buffer-string))))
          ((eq style 'tree)
           (file-name-nondirectory (directory-file-name entry)))
          (t entry)))

  (setq org-publish-project-alist
	`(("org"
	   :base-directory ,(expand-file-name "articles" org-directory)
	   :base-extension "org"
	   :recursive t
	   :publishing-function mine/org-html-publish-to-html
	   :publishing-directory ,(expand-file-name "Publish" "~")
	   :section-numbers nil
	   :with-toc nil
	   :html-head ,mine/website-html-head
	   :exclude "level-.*\\|.*\.draft\.org"
	   :html-preamble ,mine/website-html-preamble
	   :html-postamble ,mine/website-html-postamble
	   :with-title t
	   :with-date t
	   :with-tags t
	   :html-doctype "html5"
	   :html-html5-fancy t
	   :html-head-include-default-style nil
	   :html-head-include-scripts nil
	   :htmlized-source t
	   :auto-sitemap t
	   :sitemap-filename "index.org"
	   :sitemap-title "🖥 📝 Sriharsha - Personal Website"
	   :sitemap-style list
	   :sitemap-sort-files anti-chronologically
	   :sitemap-function mine/org-html-sitemap-function
	   :sitemap-format-entry mine/org-html-sitemap-format-entry)
	  ;; TODO: Generated img tag should be like this /assets/images/<>
	  ("images"
	   :base-directory ,(expand-file-name "assets/images" (expand-file-name "articles" org-directory))
	   :base-extension "jpg\\|gif\\|png"
	   :recursive t
	   :publishing-directory ,(expand-file-name "assets/images" (expand-file-name "Publish" "~"))
	   :publishing-function org-publish-attachment)

	  ("js"
	   :base-directory ,(expand-file-name "assets/js" (expand-file-name "articles" org-directory))
	   :base-extension "js"
	   :recursive t
	   :publishing-directory ,(expand-file-name "assets/js" (expand-file-name "Publish" "~"))
	   :publishing-function org-publish-attachment)

	  ("css"
	   :base-directory ,(expand-file-name "assets/css" (expand-file-name "articles" org-directory))
	   :base-extension "css"
	   :recursive t
	   :publishing-directory ,(expand-file-name "assets/css" (expand-file-name "Publish" "~"))
	   :publishing-function org-publish-attachment)

	  ("rss"
	   :base-directory ,(expand-file-name "articles" org-directory)
	   :exclude "level-.*\\|.*\.draft\.org"
	   :publishing-directory ,(expand-file-name "Publish" "~")
	   :base-extension "org"
	   :html-link-home "https://nerding_it.gitlab.io"
	   :html-link-use-abs-url t
	   :rss-extension "xml"
	   :publishing-function mine/org-rss-publish-to-rss
	   :html-link-org-files-as-html t
	   :auto-sitemap t
	   :sitemap-filename "index.org"
	   :sitemap-title "🖥 📝 Sriharsha - Personal Website"
	   :sitemap-style list
	   :sitemap-sort-files anti-chronologically
	   :sitemap-function mine/org-rss-sitemap-function
	   :sitemap-format-entry mine/org-rss-sitemap-format-entry
	   :recursive t)

	  ("website" :components ("org" "images" "js" "css" "rss"))))
  (setq org-html-validation-link nil))

(defun org-babel-execute:plantuml (body params)
  "Execute a block of plantuml code with org-babel.
This function is called by `org-babel-execute-src-block'."
  (let* ((out-file (or (cdr (assq :file params))
		       (error "PlantUML requires a \":file\" header argument")))
	 (cmdline (cdr (assq :cmdline params)))
	 (in-file (org-babel-temp-file "plantuml-"))
	 (java (or (cdr (assq :java params)) ""))
	 (executable (cond ((eq org-plantuml-exec-mode 'plantuml) org-plantuml-executable-path)
			   (t "plantuml")))
	 (full-body (org-babel-plantuml-make-body body params))
	 (cmd (mapconcat #'identity
			 (append
			  (list executable)
			  (pcase (file-name-extension out-file)
			    ("png" '("-tpng"))
			    ("svg" '("-tsvg"))
			    ("eps" '("-teps"))
			    ("pdf" '("-tpdf"))
			    ("tex" '("-tlatex"))
			    ("vdx" '("-tvdx"))
			    ("xmi" '("-txmi"))
			    ("scxml" '("-tscxml"))
			    ("html" '("-thtml"))
			    ("txt" '("-ttxt"))
			    ("utxt" '("-utxt")))
			  (list
			   "-p"
			   cmdline
			   "<"
			   (org-babel-process-file-name in-file)
			   ">"
			   (org-babel-process-file-name out-file)))
			 " ")))
    (with-temp-file in-file (insert full-body))
    (message "%s" cmd) (org-babel-eval cmd "")
    nil))

(provide 'config-publish)

;;; config-publish.el ends here
