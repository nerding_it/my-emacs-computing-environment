;;; config-python.el --- Configuration for python-mode

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 19 Nov 2020
;; Keywords: python, Emacs

;;; Commentary:

;; This is configuration related to python-mode

;;; Code:

(use-package flymake-python-pyflakes
  :ensure)

(use-package elpy
  :ensure)

(use-package python
  :requires flymake-python-pyflakes
  :config
  (when (not (executable-find "pyflakes"))
    ;; Check `pyflakes' is present, if not install using pip
    (if (not (executable-find "pip"))
	;; Throw message if pip is not installed
	(message "Please install pip first")
      (shell-command-to-string "pip install pyflakes")))
  (org-babel-do-load-languages 'org-babel-load-languages
			       (append org-babel-load-languages
				       '((python . t))))
  :hook
  (python-mode . flymake-python-pyflakes-load)
  (python-mode . elpy-enable))

(use-package pip-requirements
  :ensure)

(use-package pyvenv
  :ensure)

(provide 'config-python)

;;; config-python.el ends here
