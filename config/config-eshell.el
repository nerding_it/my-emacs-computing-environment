;;; config-eshell.el --- Configure eshell for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: eshell, Emacs

;;; Commentary:

;; This is configuration related to eshell

;;; Code:

(use-package eshell
  :bind (("C-c h" . (lambda ()
		      "Complete the history"
		      (interactive)
		      (insert
		       (ido-completing-read "Eshell history: "
					    (delete-dups
					     (ring-elements eshell-history-ring))))))
	 ("C-c C-h" . 'eshell-list-history))
  :config
  (setenv "PAGER" "cat")
  (if (file-exists-p (expand-file-name ".local/bin" "~"))
      (eshell/addpath (expand-file-name ".local/bin" "~")))
  :custom
  (eshell-aliases-file (expand-file-name ".eshell_aliases" user-emacs-directory)))

(provide 'config-eshell)

;;; config-eshell.el ends here
