;;; config-crypt.el --- Configure cryptography related stuffs for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: crypt, elegant

;;; Commentary:

;; This is configuration related to cryptography

;;; Code:

(use-package epa
  :after emacs
  :custom
  (epa-file-encrypt-to user-mail-address)
  (epa-pinentry-mode 'loopback)
  (epa-file-select-keys nil))

(use-package org-crypt
  :after (org epa)
  :config
  (org-crypt-use-before-save-magic)
  :custom
  (org-crypt-key epa-file-encrypt-to)
  (auto-save-default nil)
  (org-tags-exclude-from-inheritance '("crypt"))
  (org-crypt-key epa-file-encrypt-to))

(use-package keychain-environment
  :ensure t
  :init
  (keychain-refresh-environment))

(provide 'config-crypt)

;;; config-crypt.el ends here
