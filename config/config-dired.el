;;; config-dired.el --- Configure dired for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: dired

;;; Commentary:

;; This is configuration related to dired mode

;;; Code:

(use-package dired
  :bind (:map dired-mode-map
		("r" . dired-do-find-regexp))
  :custom
  (dired-listing-switches "-alGhvF"))

(provide 'config-dired)

;;; config-dired.el ends here
