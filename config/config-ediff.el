;;; config-ediff.el --- Configure ediff for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: ediff, elegant

;;; Commentary:

;; This is configuration related to ediff

;;; Code:

(use-package ediff
  :custom
  (ediff-window-setup-function 'ediff-setup-windows-plain))

(provide 'config-ediff)

;;; config-ediff.el ends here
