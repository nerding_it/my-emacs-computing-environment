;;; config-time.el --- Configure time for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: time, elegant

;;; Commentary:

;; This is configuration related to time

;;; Code:

(use-package time
  :custom
  (display-time-24hr-format t)
  (display-time-use-mail-icon t)
  (display-time-day-and-date nil)
  (display-time-world-list '(("Asia/Calcutta" "Bengaluru")
			     ("America/New_York" "New York")
			     ("America/Los_Angeles" "Seatle")))
  :config
  (display-time-mode))

(use-package timeclock
  :bind (:map ctl-x-map
	      ("ti" . timeclock-in)
	      ("to" . timeclock-out)
	      ("tc" . timeclock-change)
	      ("tr" . timeclock-reread-log)
	      ("tu" . timeclock-update-mode-line)
	      ("tw" . timeclock-when-to-leave-string))
  :config
  (timeclock-mode-line-display))

(provide 'config-time)

;;; config-time.el ends here
