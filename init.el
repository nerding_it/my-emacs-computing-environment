;;; init.el --- Entry point for  configuration

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: Emacs, elegant

;;; Commentary:

;; This is configuration is an entry point for actual configuration

;;; Code:


(load (expand-file-name "utils.el" user-emacs-directory))
(require 'utils)

(utils-log "INFO" "Started loading the configuration")
(utils-log "INFO" "Declaring custom variables")

(defgroup mine nil
  "Custom configuration for mine"
  :group 'local)

(defcustom mine-appearance
  'elegant
  "The appearance to use."
  :type '(radio
	  (const :tag "default" default)
	  (const :tag "elegant" elegant))
  :group 'mine)

(defcustom mine-cache-directory
  (expand-file-name ".cache/" user-emacs-directory)
  "The storage location for various persistent files."
  :type 'directory
  :group 'mine)

(defcustom mine-second-brain-location
  (expand-file-name "Org/" "~")
  "The location for second brain."
  :type 'directory
  :group 'mine)

(defcustom mine-window-manager
  t
  "Whether emacs should be used as window manager or not."
  :type 'boolean
  :group 'mine)

(defcustom mine-git-directory
  (expand-file-name "Git" "~")
  "This is the directory where I keep my project directories."
  :type 'directory
  :group 'mine)

(utils-log "INFO" "Initializing packages")
;; Make sure `use-package' is installed
;; Else install from elpa
(require 'package)
(package-initialize)
(when (not (package-installed-p 'use-package))
  (utils-log "INFO" "Use package is not installed so installing")
  (let ((package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
			    ("melpa" . "https://melpa.org/packages/"))))
    (package-refresh-contents)
    (package-install 'use-package)))

(require 'use-package)

(use-package cus-edit
  :demand t
  :custom
  (custom-file (expand-file-name "custom.el" user-emacs-directory))
  :init
  (when (file-exists-p custom-file)
    (utils-log "INFO" "Loading custom.el")
    (load custom-file)))

(let ((config-directory (expand-file-name "site-lisp/" user-emacs-directory)))
  (load (expand-file-name "ox-rss.el" config-directory))
  (require 'ox-rss))

(let ((config-directory (expand-file-name "config/" user-emacs-directory)))
  (load (expand-file-name "config-core.el" config-directory))
  (load (expand-file-name "config-tls.el" config-directory))
  (load (expand-file-name "config-package.el" config-directory))
  (load (expand-file-name "config-wm.el" config-directory))
  (load (expand-file-name "config-appearance.el" config-directory))
  (load (expand-file-name "config-symbols.el" config-directory))
  (load (expand-file-name "config-time.el" config-directory))
  (load (expand-file-name "config-battery.el" config-directory))
  (load (expand-file-name "config-dired.el" config-directory))
  (load (expand-file-name "config-eshell.el" config-directory))
  (load (expand-file-name "config-crypt.el" config-directory))
  (load (expand-file-name "config-desktop.el" config-directory))
  (load (expand-file-name "config-prog.el" config-directory))
  (load (expand-file-name "config-autoinsert" config-directory))
  (load (expand-file-name "config-org.el" config-directory))
  (load (expand-file-name "config-emms.el" config-directory))
  (load (expand-file-name "config-buffer.el" config-directory))
  (load (expand-file-name "config-ido.el" config-directory))
  (load (expand-file-name "config-windows.el" config-directory))
  (load (expand-file-name "config-calendar.el" config-directory))
  (load (expand-file-name "config-newsticker.el" config-directory))
  (load (expand-file-name "config-gnus.el" config-directory))
  (load (expand-file-name "config-grammar.el" config-directory))
  (load (expand-file-name "config-web.el" config-directory))
  (load (expand-file-name "config-misc.el" config-directory))
  (load (expand-file-name "config-abbrev.el" config-directory))
  (load (expand-file-name "config-ediff.el" config-directory))
  (load (expand-file-name "config-vc.el" config-directory))
  (load (expand-file-name "config-rust.el" config-directory))
  (load (expand-file-name "config-tex.el" config-directory))
  (load (expand-file-name "config-erc.el" config-directory))
  (load (expand-file-name "config-daily" config-directory))
  (load (expand-file-name "config-project" config-directory))
  (load (expand-file-name "config-system" config-directory))
  (load (expand-file-name "config-conf" config-directory))
  (load (expand-file-name "config-speedbar" config-directory))
  (load (expand-file-name "config-ledger" config-directory))
  (load (expand-file-name "config-devops" config-directory))
  (load (expand-file-name "config-tags" config-directory))
  (load (expand-file-name "config-python" config-directory))
  (load (expand-file-name "config-imenu" config-directory))
  (load (expand-file-name "config-request" config-directory))
  (load (expand-file-name "config-publish" config-directory))
  (load (expand-file-name "config-info" config-directory))
  (load (expand-file-name "config-proced" config-directory))
  (load (expand-file-name "config-quelpa" config-directory))
  (require 'config-core)
  (require 'config-calendar)
  (require 'config-tls)
  (require 'config-package)
  (require 'config-quelpa)
  (require 'config-wm)
  (require 'config-appearance)
  (require 'config-symbols)
  (require 'config-time)
  (require 'config-battery)
  (require 'config-dired)
  (require 'config-eshell)
  (require 'config-crypt)
  (require 'config-windows)
  (require 'config-desktop)
  (require 'config-prog)
  (require 'config-org)
  (require 'config-system)
  (require 'config-emms)
  (require 'config-ido)
  (require 'config-buffer)
  (require 'config-newsticker)
  (require 'config-gnus)
  (require 'config-grammar)
  (require 'config-web)
  (require 'config-misc)
  (require 'config-abbrev)
  (require 'config-ediff)
  (require 'config-vc)
  (require 'config-rust)
  (require 'config-tex)
  (require 'config-daily)
  (require 'config-publish)
  (require 'config-project)
  (require 'config-erc)
  (require 'config-autoinsert)
  (require 'config-speedbar)
  (require 'config-ledger)
  (require 'config-devops)
  (require 'config-tags)
  (require 'config-python)
  (require 'config-imenu)
  (require 'config-request)
  (require 'config-info)
  (require 'config-proced)
  (require 'config-conf))

(utils-log "INFO" "Loaded configuration files")

;;; init.el ends here
