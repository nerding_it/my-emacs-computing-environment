;;; config-quelpa.el --- Package directly loaded from quelpa

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created:  7 Jul 2021
;; Keywords: Quelpa, Emacs

;;; Commentary: 

;; Configuration for additional packages

;;; Code:

(use-package topcoder-extension
  :quelpa ((topcoder-extension
	    :fetcher gitlab
	    :repo "nerding_it/emacs-topcoder-extension")
	   :upgrade nil))

(provide 'config-quelpa)

;;; config-quelpa.el ends here
