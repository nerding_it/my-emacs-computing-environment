;;; config-appearance.el --- Configure appearance for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: appearance, elegant

;;; Commentary:

;; This is configuration related to appearance for Emacs.

;;; Code:

(use-package scroll-bar
  :config
  (scroll-bar-mode 0))

(use-package menu-bar
  :config
  (menu-bar-mode -1))

(use-package tool-bar
  :config
  (tool-bar-mode -1))

(use-package fringe
  :config
  (fringe-mode '(nil . 0)))

(use-package frame
  :config
  (blink-cursor-mode 0))

(use-package all-the-icons
  :if (eq mine-appearance 'elegant)
  :ensure t)

(use-package all-the-icons-dired
  :if (eq mine-appearance 'elegant)
  :requires all-the-icons
  :after (dired all-the-icons)
  :ensure t
  :hook (dired-mode . all-the-icons-dired-mode))

(use-package dashboard
  :if (eq mine-appearance 'elegant)
  :requires all-the-icons
  :ensure t
  :custom
  (dashboard-banner-logo-title "🄦🄔 ♥ ⒺⓂⒶⒸⓢ")
  (dashboard-startup-banner "~/Pictures/gnu_logo.png")
  (dashboard-center-content t)
  (dashboard-show-shortcuts nil)
  (dashboard-items '((recents . 5)
  		     (bookmarks . 5)
  		     (agenda . 5)
  		     (registers . 5)))
  (dashboard-set-heading-icons t)
  (dashboard-set-file-icons t)
  (dashboard-set-navigator t)
  (dashboard-set-init-info t)
  (dashboard-set-footer nil)
  (show-week-agenda-p t)
  (initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))
  :config
  (dashboard-setup-startup-hook))

(use-package poet-theme
  :if (eq mine-appearance 'elegant)
  :ensure t
  :demand t
  :init
  (set-face-attribute 'default nil :family "Iosevka" :height 130)
  (set-face-attribute 'fixed-pitch nil :family "Iosevka")
  (set-face-attribute 'variable-pitch nil :family "Iosevka")
  (load-theme 'poet-dark t))

(use-package smart-mode-line
  :ensure t
  :custom
  (sml/no-confirm-load-theme t)
  (sml/theme 'respectful)
  :config
  (sml/setup))

(use-package olivetti
  :if (eq mine-appearance 'elegant)
  :ensure t
  :after (poet-theme org)
  :custom
  (olivetti-body-width 100)
  :hook (org-mode . olivetti-mode))

(use-package org-bullets
  :ensure t
  :after org
  :hook (org-mode . org-bullets-mode))

(use-package custom
  :if (eq mine-appearance 'default)
  :config
  (set-face-attribute 'default nil :family "Hack" :height 140)
  (set-face-attribute 'fixed-pitch nil :family "Hack")
  (set-face-attribute 'variable-pitch nil :family "Hack")
  (load-theme 'adwaita))

(use-package custom
  :if (eq mine-appearance 'elegant)
  :config
  (set-face-attribute 'default nil :family "Iosevka" :height 140)
  (set-face-attribute 'fixed-pitch nil :family "Iosevka")
  (set-face-attribute 'variable-pitch nil :family "Iosevka")
  (load-theme 'poet-dark))

(provide 'config-appearance)

;;; config-appearance.el ends here
