;;; config-org.el --- Configure org-mode for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: org, elegant

;;; Commentary:

;; This is configuration related to org mode

;; TODO: Improving tags
;; TODO: Improving review configuration
;; TODO: Implementing auto clock by tags, effort and invoice report generation

;;; Code:

(use-package org
  :hook (org-mode . (lambda ()
		      (local-set-key (kbd "C-c b") 'mine/org-babel-skeleton)))
  :config
  (global-set-key (kbd "C-c c") 'org-capture)
  (global-set-key (kbd "C-c a") 'org-agenda)
  :custom
  (org-confirm-babel-evaluate nil)
  (org-directory mine-second-brain-location)
  (diary-file (expand-file-name "diary" org-directory))
  (org-use-speed-commands t)
  (org-agenda-skip-deadline-if-done t)
  (org-agenda-skip-scheduled-if-done t)
  (org-agenda-skip-timestamp-if-done t)
  (org-agenda-start-on-weekday 1)

  ;; Agenda files
  (org-agenda-files (list
		     (expand-file-name "inbox.org" org-directory)
		     (expand-file-name "gtd.org" org-directory)
		     (expand-file-name "tickler.org" org-directory)))
  ;; Add UTF-8 Symbols
  (org-tag-alist '((:startgrouptag)
		   (:grouptags)
		   ("@work" . ?w)
		   ("@home" . ?h)
		   ("@outdoor" . ?o)
		   (:endgrouptag)
		   (:grouptags)
		   ("discussion" . ?d)
		   ("meeting" . ?m)
		   ("marketing" . ?M)
		   (:grouptags)
		   ("research" . ?R)
		   ("coding" . ?C)
		   ("writing" . ?W)
		   ("review" . ?r)
		   ("improvement" . ?i)
		   ("life" . ?l)
		   ("emacs" . ?a)
		   ("followup" . ?f)
		   (:grouptags)
		   ("crypt" . ?c)
		   (:endgrouptag)
		   (:grouptags)
		   ("client" . ?p)
		   ("contribution" . ?O)
		   (:endgrouptag)
		   (:grouptags)
		   ("WAITING" . ?W)
		   ("CANCELLED" . ?C)
		   (:endgrouptag)))

  (org-use-fast-todo-selection t)
  ;; TODO -> DONE
  ;; WAIT -> STOP
  (org-todo-keywords '((sequence "TODO(t)" "|" "DONE(d)")
		       (sequence "WAIT(w@)" "|" "STOP(c@)")))
  (org-todo-state-tags-triggers '(
				  ;; Moving to wait adds wait
				  ("WAIT" ("WAITING" . t))
				  ;; Moving to stop adds stop, removes wait
				  ("STOP" ("CANCELLED" . t))
				  ;; Moving to done removes wait/stop
				  ("DONE" ("WAITING") ("CANCELLED"))
				  ;; Moving to todo removes wait/stop
				  ("TODO" ("WAITING") ("CANCELLED"))))

  (org-crypt-use-before-save-magic)
  (org-tags-exclude-from-inheritance '("crypt"))
  (org-agenda-include-diary t)
  (org-refile-targets '((org-agenda-files :maxlevel . 3)))


  (org-capture-templates `(
			   ("t" "TODO"
			    entry
			    (file+headline ,
			     (expand-file-name "inbox.org" org-directory) "TASKS")
			    "* TODO %?\n %i\n")
			   ("p" "POST"
			    plain
			    (file mine/capture-article)
			    (function mine/capture-article-template))))
  :config
  (defun mine/capture-article ()
    "Function to capture `article' in `org-mode'."
    (let* ((article-base-directory (expand-file-name "articles" org-directory))
  	   (article-directory (expand-file-name (format-time-string "%Y/%m/%d" (current-time)) article-base-directory))
  	   (title nil)
  	   (slug nil))
      (unless (file-exists-p article-directory)
      	(make-directory article-directory t))
      (setq title (read-string "Title: "))
      (setq slug (replace-regexp-in-string "[^a-z]+" "_" (downcase title)))
      (expand-file-name (concat slug ".org") article-directory)))

  (defun mine/capture-article-template ()
    (concat "#+TITLE: %^{Title}\n"
	    "#+DATE: %<%Y-%m-%d>")))

(use-package org-mind-map
  :ensure
  :after org
  :init
  (require 'ox-org)
  :custom
  (org-mind-map-engine "dot"))

(use-package org-agenda
  :config
  (add-to-list 'org-agenda-custom-commands
	       '("R" . "Review"))
  (setq mine/org-agenda-review-settings
        '((org-agenda-show-all-dates t)
          (org-agenda-start-with-log-mode t)
          (org-agenda-start-with-clockreport-mode t)
          (org-agenda-archives-mode t)
          ;; I don't care if an entry was archived
          (org-agenda-hide-tags-regexp
           (concat org-agenda-hide-tags-regexp
                   "\\|ARCHIVE"))))
  (add-to-list 'org-agenda-custom-commands
               `("Rd" "Day in review"
                 agenda ""
                 ;; agenda settings
                 ,(append
                   mine/org-agenda-review-settings
                   '((org-agenda-span 'day)
                     (org-agenda-overriding-header "Day in Review"))
                   )
                 ("~/Publish/review/day.html"))))


(use-package org-pomodoro
  :ensure
  :after org)

(provide 'config-org)

;;; config-org.el ends here
