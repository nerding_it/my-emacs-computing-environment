;;; config-ido.el --- Configure abbreviation, completion for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: ido, elegant

;;; Commentary:

;; This is configuration related to ido mode

;;; Code:

(use-package registers-completing-read
  :quelpa ((registers-completing-read
	    :fetcher gitlab
	    :repo "nerding_it/emacs-registers-completing-read")
	   :upgrade nil))

(defun mine/save-current-file-to-register ()
  "Save current file to register."
  (interactive)
  (let ((reg (register-read-with-preview "File name to register: ")))
    (set-register reg `(file . ,(buffer-file-name)))))
  
(defun mine/ido-jump-to-bookmark (bookmark)
  "Jump to bookmark `BOOKMARK'."
  (interactive (list (ido-completing-read "Bookmark: " (bookmark-all-names) nil t)))
  (bookmark-jump bookmark))

(defun mine/ido-kill-ring (ring)
  "Insert text in `RING'."
  (interactive (list (ido-completing-read "ring: " kill-ring)))
  (insert-for-yank ring))

(use-package ido
  :demand t
  :bind (("C-x b" . ido-switch-buffer)
	 ("C-x r b" . mine/ido-jump-to-bookmark)
	 ("C-x r j" . registers-completing-read-jump)
	 ("C-x r i" . registers-completing-read-insert)
	 ("C-x r a" . append-to-register)
	 ("C-x r F" . append-to-register)
	 ("C-x y" . mine/ido-kill-ring))
  :custom
  (ido-enable-prefix nil)
  (ido-file-extensions-order
   '(".org" ".md" ".py" ".el" ".ts" ".js" ".html" ".scss" ".css"))
  (ido-use-virtual-buffers t)
  (ido-enable-flex-matching t)
  (ido-create-new-buffer 'always)
  (ido-use-filename-at-point 'guess)
  (ido-save-directory-list-file (expand-file-name "ido.last" mine-cache-directory))
  :config
  (set-face-attribute 'ido-first-match nil
		      :foreground "white"
		      :background "DeepSkyBlue1"
		      :weight 'bold)
  (ido-everywhere t)
  (ido-mode 1))

(use-package icomplete
  :demand t
  :bind (:map icomplete-minibuffer-map
		("M-TAB" . icomplete-force-complete)
		("C-s" . icomplete-forward-completions)
		("C-p" . icomplete-backward-completions)
		("C-j" . icomplete-force-complete-and-exit))
  :custom
  (icomplete-in-buffer t)
  (icomplete-delay-completions-threshold 0)
  (icomplete-compute-delay 0)
  (icomplete-max-delay-chars 0)
  (icomplete-show-matches-on-no-input t)
  (icomplete-hide-common-prefix nil)
  (completion-cycle-threshold 10)
  :hook (icomplete-minibuffer-setup . (lambda ()
					(setq completion-styles '(initials partial-completion flex))))
  :config
  (icomplete-mode))

(provide 'config-ido)

;;; config-ido.el ends here
