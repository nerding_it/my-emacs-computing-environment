;;; config-emms.el --- Configure emms for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: emms, elegant

;;; Commentary:

;; This is configuration related to emms

;;; Code:

(use-package emms
  :ensure t
  :config
  (require 'emms-setup)
  (require 'emms-player-simple)
  (require 'emms-streams)
  (require 'emms-librefm-stream)
  (require 'emms-player-mplayer)
  (require 'emms-source-file)
  (require 'emms-source-playlist)
  (emms-standard)
  (emms-default-players)
  :custom
  (emms-playlist-buffer-name "*Music*")
  (emms-player-list '(emms-player-mplayer emms-player-ogg123 emms-player-mpg321)))

(provide 'config-emms)

;;; config-emms.el ends here
