;;; config-abbrev.el --- Configure abbreviation, completion for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: abbrev, elegant

;;; Commentary:

;; This is configuration related to abbreviation feature of Emacs.

;;; Code:

(use-package hippie-exp
  :custom
  (hippie-expand-try-functions-list
   '(try-complete-file-name-partially
     try-complete-file-name
     try-expand-dabbrev
     try-expand-dabbrev-all-buffers
     try-expand-dabbrev-from-kill
     try-complete-lisp-symbol))
  :bind
  (("M-SPC" . hippie-expand)))

(use-package dabbrev
  :init
  (unbind-key "M-/"))

(use-package completion
  :bind ("M-/" . completion-at-point))

(use-package abbrev
  :hook ((prog-mode . abbrev-mode)
	 (text-mode . abbrev-mode)))

(provide 'config-abbrev)

;;; config-abbrev.el ends here
