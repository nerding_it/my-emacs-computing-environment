;;; config-misc.el --- Configure misc stuff for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: misc, elegant

;;; Commentary:

;; This is configuration related to misc

;;; Code:

(use-package debbugs
  :ensure t)

(use-package which-key
  :ensure t
  :config
  (which-key-mode))

(defun mine/toggle-webcam ()
  "Toggle webcam, it'll open a new buffer with webcam preview"
  (interactive)
  (if (get-buffer "ffplay")
      (kill-buffer (get-buffer "ffplay")))
  (start-process "ffplay" nil "ffplay" "/dev/video0"))

(defun mine/generate-ssh-keypair ()
  "Generate new ssh keypairs."
  (interactive)
  (let ((email (read-string "Email: ")))
    (async-shell-command
     (format "ssh-keygen -t rsa -b 4096 -C \"%s\"" email))))

(defun mine/generate-gpg-keypair ()
  "Generate new gpg keypairs."
  (interactive)
  (async-shell-command "gpg --full-generate-key"))

(use-package vlf
  :ensure)

(setq source-directory (expand-file-name "emacs/src" mine-git-directory))

(provide 'config-misc)

;;; config-misc.el ends here
