;;; config-autoinsert.el --- Configure autoinsert for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: autoinsert, elegant

;;; Commentary:

;; This is configuration related to auto insert

;;; Code:


(use-package skeleton
  :config
  (define-skeleton mine/org-babel-skeleton
    "Org babel skeleton."
    ""
    "#+NAME: " (skeleton-read "Name: ") \n
    "#+BEGIN_SRC " (skeleton-read "Babel: ") \n
    -
    \n "#+END_SRC")
  (define-skeleton mine/emacs-lisp-doc-comment-skeleton
    "Emacs lisp doc string skeleton."
    ";;; "
    (format ";;; %s"(file-name-nondirectory (buffer-file-name))) " --- " (skeleton-read "Title: ") \n
    "" \n
    ";; Author: " (format "%s <%s>" user-full-name user-mail-address)\n
    ";; Created: " (format-time-string "%e %b %Y")\n
    ";; Keywords: " (skeleton-read "Keywords: ") \n
    ""\n
    ";;; Commentary: " \n
    "" \n
    ";; " (skeleton-read "Description: ") \n
    "" \n
    ";;; Code:" \n
    "" \n
    > _ \n
    "" \n
    ";;; " (format "%s ends here" (file-name-nondirectory (buffer-file-name)))))


(use-package autoinsert
  :requires (elisp-mode files)
  :config
  (define-auto-insert '(emacs-lisp-mode . "Emacs lisp skeleton") 'mine/emacs-lisp-doc-comment-skeleton)
  :init
  (auto-insert-mode))

(provide 'config-autoinsert)

;;; config-autoinsert.el ends here
