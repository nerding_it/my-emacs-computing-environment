;;; config-vc.el --- Configure Version control for elegant-computing-environment

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 01 Oct 2020
;; Keywords: vc, version control, elegant

;;; Commentary:

;; This is configuration related to vc package

;;; Code:

(use-package vc)

(use-package vc-hooks
  :requires (vc)
  :config
  (define-key vc-prefix-map "p" 'vc-pull)
  (define-key vc-prefix-map "f" 'vc-log-search))

(use-package magit
  :ensure
  :config
  (define-key vc-prefix-map "e" 'magit-rebase))

(provide 'config-vc)

;;; config-vc.el ends here
